package ru.ermolaev.tm.api.repository;

import ru.ermolaev.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    List<Project> findAll();

    void remove(Project project);

    void clear();

    Project findById(String id);

    Project findByIndex(Integer index);

    Project findByName(String name);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project removeByName(String name);

}
