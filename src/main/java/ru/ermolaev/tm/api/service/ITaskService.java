package ru.ermolaev.tm.api.service;

import ru.ermolaev.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void createTask(String name);

    void createTask(String name, String description);

    void addTask(Task task);

    List<Task> showAllTasks();

    void removeTask(final Task task);

    void removeAllTasks();

    Task findTaskById(String id);

    Task findTaskByIndex(Integer index);

    Task findTaskByName(String name);

    Task updateTaskById(String id, String name, String description);

    Task updateTaskByIndex(Integer index, String name, String description);

    Task removeTaskById(String id);

    Task removeTaskByIndex(Integer index);

    Task removeTaskByName(String name);

}
