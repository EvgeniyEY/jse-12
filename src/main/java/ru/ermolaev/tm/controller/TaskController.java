package ru.ermolaev.tm.controller;

import ru.ermolaev.tm.api.controller.ITaskController;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.model.Task;
import ru.ermolaev.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.showAllTasks();
        int index = 1;
        for (Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.removeAllTasks();
        System.out.println("[COMPLETE]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        taskService.createTask(name, description);
        System.out.println("[COMPLETE]");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findTaskById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findTaskByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findTaskByName(name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findTaskById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskById(id, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findTaskByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskByIndex(index, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeTaskById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.removeTaskByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

    @Override
    public void removeTaskByName() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeTaskByName(name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

}
