package ru.ermolaev.tm.controller;

import ru.ermolaev.tm.api.controller.IProjectController;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.model.Project;
import ru.ermolaev.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[PROJECTS LIST]");
        final List<Project> projects = projectService.showAllProjects();
        int index = 1;
        for (Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.removeAllProjects();
        System.out.println("[COMPLETE]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.createProject(name, description);
        System.out.println("[COMPLETE]");
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findProjectById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.findProjectByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findProjectByName(name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findProjectById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectById(id, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.findProjectByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectByIndex(index, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeProjectById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.removeProjectByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeProjectByName(name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

}
