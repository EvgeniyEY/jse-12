package ru.ermolaev.tm.constant;

public interface CommandConst {

    String HELP = "help";

    String ABOUT = "about";

    String VERSION = "version";

    String INFO = "info";

    String EXIT = "exit";

    String COMMANDS = "commands";

    String ARGUMENTS = "arguments";

    String TASK_CREATE = "task-create";

    String TASK_DELETE = "task-delete";

    String TASK_LIST = "task-list";

    String TASK_SHOW_BY_ID = "task-show-by-id";

    String TASK_SHOW_BY_INDEX = "task-show-by-index";

    String TASK_SHOW_BY_NAME = "task-show-by-name";

    String TASK_UPDATE_BY_ID = "task-update-by-id";

    String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    String TASK_REMOVE_BY_ID = "task-remove-by-id";

    String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    String TASK_REMOVE_BY_NAME = "task-remove-by-name";

    String PROJECT_CREATE = "project-create";

    String PROJECT_DELETE = "project-delete";

    String PROJECT_LIST = "project-list";

    String PROJECT_SHOW_BY_ID = "project-show-by-id";

    String PROJECT_SHOW_BY_INDEX = "project-show-by-index";

    String PROJECT_SHOW_BY_NAME = "project-show-by-name";

    String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

}
