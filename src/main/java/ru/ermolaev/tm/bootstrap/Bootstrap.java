package ru.ermolaev.tm.bootstrap;

import ru.ermolaev.tm.api.controller.ICommandController;
import ru.ermolaev.tm.api.controller.IProjectController;
import ru.ermolaev.tm.api.controller.ITaskController;
import ru.ermolaev.tm.api.repository.ICommandRepository;
import ru.ermolaev.tm.api.repository.IProjectRepository;
import ru.ermolaev.tm.api.repository.ITaskRepository;
import ru.ermolaev.tm.api.service.ICommandService;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.constant.ArgumentConst;
import ru.ermolaev.tm.constant.CommandConst;
import ru.ermolaev.tm.controller.CommandController;
import ru.ermolaev.tm.controller.ProjectController;
import ru.ermolaev.tm.controller.TaskController;
import ru.ermolaev.tm.repository.CommandRepository;
import ru.ermolaev.tm.repository.ProjectRepository;
import ru.ermolaev.tm.repository.TaskRepository;
import ru.ermolaev.tm.service.CommandService;
import ru.ermolaev.tm.service.ProjectService;
import ru.ermolaev.tm.service.TaskService;
import ru.ermolaev.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args) {
        System.out.println("Welcome to task manager");
        if (parseArgs(args)) System.exit(0);
        while (true) parseCommand(TerminalUtil.nextLine());
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseArg(final String arg) {
        if (arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                System.out.println("Invalid argument");
        }
    }

    public void parseCommand(final String arg) {
        if (arg.isEmpty()) return;
        switch (arg) {
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.INFO:
                commandController.showInfo();
                break;
            case CommandConst.EXIT:
                commandController.exit();
            case CommandConst.COMMANDS:
                commandController.showCommands();
                break;
            case CommandConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_DELETE:
                taskController.clearTasks();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_DELETE:
                projectController.clearProjects();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case CommandConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CommandConst.TASK_SHOW_BY_NAME:
                taskController.showTaskByName();
                break;
            case CommandConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CommandConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CommandConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CommandConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CommandConst.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case CommandConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case CommandConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConst.PROJECT_SHOW_BY_NAME:
                projectController.showProjectByName();
                break;
            case CommandConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CommandConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CommandConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            default:
                System.out.println("Invalid command");
        }
    }

}
